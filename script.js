// Variables
let playing = false;
let moves = 0;
let conditionX = "false";
let conditionY = "false";
let func = "0";

let start = document.getElementById("reset");
start.addEventListener('click' ,startResetGame);

let selectPartner = document.getElementById("player-number");
selectPartner.addEventListener('click', selectOpponent);

let blocks = document.getElementsByClassName("box");
blocks = Array.from(blocks);
//console.log(blocks);
//console.log(blocks[0].textContent);

let twoPlayers = document.getElementById("2-player");
twoPlayers.addEventListener('click', twoPlayerGame);

let onePlayer = document.getElementById("1-player");
onePlayer.addEventListener('click', onePlayerGame);

const winningConditions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
];

//Event Registrations
//document.body.addEventListener('click', onClick);

// Helper Functions
function setText(id, text) {
    document.getElementById(id).innerHTML = text;
}

function show(id) {
    document.getElementById(id).style.display = 'block';
}

function hide(id) {
    document.getElementById(id).style.display = 'none';
}


//functions

function twoPlayerGame(e){
    func = "2";
    player1 = prompt("Enter Player1's name: ");
    console.log(player1);
    player2 = prompt("Enter Player2's name: ");
    console.log(player2);
    hide("select-opponent");
    document.body.addEventListener('click', onClick);
}
function onePlayerGame(){
    func = "1";
    player1 = prompt("Enter Your name: ");
    console.log(player1);
    hide("select-opponent");
    player2 = "Computer"
//    console.log("before vsComputer is called");
    document.body.addEventListener('click', vsComputer);

}

function startResetGame(e){
    if(start.innerHTML === "Start"){
        playing = true;
    }else{
        playing = false;
    }
//    func = "0";
//    console.log(playing);
    if(playing === false){
        setText("reset", "Start");
        hide("status-msg");
        conditionX = "false";
        conditionY = "false";
        moves = 0;
        
        setText("moves","Moves: " +moves);
        blocks.forEach(function(block){
            block.textContent = "";
        });
        console.log(blocks);
        for(let i = 0; i<9 ; i++){
            console.log(blocks[i].textContent);
        }
    }else{
        //game is already started so you want to reset
        setText("reset", "Replay");
        
    }
}

// 2 player game

function onClick(e) {
    if(playing === true){
        if(e.target.classList.contains("box") && func === "2"){
            if(moves%2 === 0){
                let x = e.target;
                if(x.textContent === ""){
                    x.textContent = "X";
                    moves++;
                    setText("moves","Moves: " +moves);
                    checkX(e);
                }else{
                    alert("its already filled");
                }

            }else{
                let o = e.target;
                if(o.textContent === ""){
                    o.textContent = "O";
                    moves++;
                    setText("moves","Moves: " +moves);
                    checkY(e);
                }else{
                    alert("its already filled");
                }
            }
        }
    }
    
    
    if(moves == 9 && conditionX === "false" && conditionY === "false"){
        show("status-msg");
        setText("status-msg", "Its A Draw");
        playing = false;
    }
}


function selectOpponent(e){
    show("select-opponent");
}


function checkX(e){
//    console.log(e.target.textContent);
    for (i = 0; i < 8; i++) {
        let c = winningConditions[i];
        
        let q = blocks[c[0]].textContent;
        let r = blocks[c[1]].textContent;
        let s = blocks[c[2]].textContent;
//        console.log(q);
//        console.log(r);
//        console.log(s);
        
        if(q == r && r == s && s == "X"){
            show("status-msg");
            setText("status-msg", `${player1}` + " Won");
            conditionX = "win";
        }
        /*console.log((c[0]).textContent); 
        console.log((c[1]).textContent);
        console.log((c[2]).textContent);*/
      //  q = r = s = "";
    }
             
}
function checkY(e){
//    console.log(e.target.textContent);
    for (i = 0; i < 8; i++) {
        let c = winningConditions[i];
        
        let q = blocks[c[0]].textContent;
        let r = blocks[c[1]].textContent;
        let s = blocks[c[2]].textContent;
//        console.log(q);
//        console.log(r);
//        console.log(s);
        
        if(q == r && r == s && s == "O"){
            show("status-msg");
            setText("status-msg",`${player2}` + " Won ")
            conditionY = "win";
        }
        /*console.log((c[0]).textContent); 
        console.log((c[1]).textContent);
        console.log((c[2]).textContent);*/
        q = r = s = "";
    }
             
}





function vsComputer(e){
    console.log("vsComputer is called");
    if(playing === true){
        console.log("playing");
        if(e.target.classList.contains("box") && func === "1"){
            if(moves%2 === 0){
                let x = e.target;
                if(x.textContent === ""){
                    x.textContent = "X";
                    moves++;
                    setText("moves","Moves: " +moves);
                    checkX(e);
                    computerMove(e);
                }else{
                    alert("its already filled");
                }
            }
        }   
    }  
    if(moves == 9 && conditionX === "false" && conditionY === "false"){
        show("status-msg");
        setText("status-msg", "Its A Draw");
        playing = false;
    }
}

function computerMove(e){
    let position = Math.round((Math.random()*8));
    console.log(position);
    console.log(blocks[position]);
    if(blocks[position].textContent === ""){
        blocks[position].textContent = "O";
        moves++;
        setText("moves","Moves: " +moves);
        checkY(e);
    }else{
        for(let i = 0; i<9; i++){
            if(blocks[(position + i)% 8].textContent === ""){
                blocks[(position + i)% 8].textContent = "O";
                moves++;
                setText("moves","Moves: " +moves);
                checkY(e);
                break;
            }
        }
    }
}


